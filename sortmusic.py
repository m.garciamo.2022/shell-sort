#!/usr/bin/env python3

import sys


def order_items(songs: list, i: int, gap: int) -> list:
    clave = songs[i][0]
    valor = songs[i][1]
    form1 = i
    while form1 >= gap and songs[form1 - gap][1] < valor:
        songs[form1] = songs[form1 - gap]
        form1 -= gap
        songs[form1] = (clave, valor)
    return songs


def sort_music(songs: list) -> list:
    num = len(songs)
    div = num // 2

    while div > 0:
        for i in range(div, num):
            songs = order_items(songs, i, div)
        div //= 2

    return songs


def create_dictionary(arguments: list) -> dict:
    songs: dict = {}
    var = 0
    while var < len(arguments):
        nombre = arguments[var]
        var += 1
        if var < len(arguments):
            try:
                reproducciones = int(arguments[var])
                songs[nombre] = reproducciones
            except ValueError:
                sys.exit(f"Error: El argumento '{arguments[var]}' no es un número de reproducciones válido.")
        else:
            sys.exit("Error: Debes proporcionar un número de reproducciones para cada canción.")
        var += 1
    return songs


def main():
    args = sys.argv[1:]

    if len(args) % 2 != 0 or len(args) == 0:
        sys.exit("Error: Debes proporcionar pares de canción y número de reproducciones.")

    songs: dict = create_dictionary(args)

    sorted_songs: list = sort_music(list(songs.items()))

    sorted_dict: dict = dict(sorted_songs)
    print(sorted_dict)


if __name__ == '__main__':
    main()

